package hello;

import java.util.Map;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.stereotype.Component;
@Component
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
	@Override
	protected void afterSpringSecurityFilterChain(ServletContext servletContext) {
		Map<String, ? extends FilterRegistration> d = servletContext.getFilterRegistrations();
		
		for(Map.Entry<String, ? extends FilterRegistration> entry : d.entrySet())
		{
		System.out.println(entry.getKey());
			
		}
	}
}