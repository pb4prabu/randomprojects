package hello.data;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

public class TokenStoreUtil {
	public static TokenStore tokenStore = new InMemoryTokenStore();

	public static DefaultTokenServices tokenServices = new DefaultTokenServices();

	static {
		tokenServices.setSupportRefreshToken(true);
		tokenServices.setTokenStore(tokenStore);
	}

}
