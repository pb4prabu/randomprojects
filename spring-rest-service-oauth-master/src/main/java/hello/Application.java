/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
//	@Bean
//	 public TokenStore tokenStore() {
//	 return new JwtTokenStore(accessTokenConverter());
//	 }
//	
//	 @Bean
//	 public JwtAccessTokenConverter accessTokenConverter() {
//	 JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
//	 KeyStoreKeyFactory keyStoreKeyFactory =
//	 new KeyStoreKeyFactory(
//	 new ClassPathResource("mykeys.jks"),
//	 "mypass".toCharArray());
//	 converter.setKeyPair(keyStoreKeyFactory.getKeyPair("mykeys"));
//	 return converter;
//	 }
//	
//	 @Bean
//	 @Primary
//	 public DefaultTokenServices tokenServices() {
//		 System.out.println("haaaaaaaai");
//	 DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
//	 defaultTokenServices.setTokenStore(tokenStore());
//	 defaultTokenServices.setSupportRefreshToken(true);
//	 defaultTokenServices.setTokenEnhancer(accessTokenConverter());
//	 return defaultTokenServices;
//	 }

}
