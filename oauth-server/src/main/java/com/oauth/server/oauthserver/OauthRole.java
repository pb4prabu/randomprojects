package com.oauth.server.oauthserver;

import org.springframework.security.core.GrantedAuthority;

public enum OauthRole implements GrantedAuthority {
	
	ROLE_USER,ROLE_PARTNER,ROLE_ADMIN,ROLE_SUPER_ADMIN,ROLE_GUEST;

	@Override
	public String getAuthority() {
		return this.toString();
	}

}
