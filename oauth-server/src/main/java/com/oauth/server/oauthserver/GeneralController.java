package com.oauth.server.oauthserver;

import java.security.Principal;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GeneralController {
    @GetMapping("/api/public")
         public RestMsg hello(){
         return new RestMsg("Hello World!");
    }

    @GetMapping("/test")
         public RestMsg apitest(){
         return new RestMsg("Hello apiTest!");
    }

    @GetMapping(value = "/api/user", produces = "application/json")
    public RestMsg helloUser(){
        // The authenticated user can be fetched using the SecurityContextHolder
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        return new RestMsg(String.format("Hello user with name '%s'!", username));
    }

    @GetMapping("/api/admin")
    // If a controller request asks for the Principal user in
    // the method declaration Spring security will provide it.
    public RestMsg helloAdmin(Principal principal){
        return new RestMsg(String.format("Welcome admin with name '%s'!", principal.getName()));
    }

    
    @GetMapping("/api/partner")
    public RestMsg partner(Principal principal){
    return new RestMsg(String.format("Hi partner with name '%s'!", principal.getName()));
}
    // A helper class to make our controller output look nice
    public static class RestMsg {
        private String msg;
        public RestMsg(String msg) {
            this.msg = msg;
        }
        public String getMsg() {
            return msg;
        }
        public void setMsg(String msg) {
            this.msg = msg;
        }
    }
}
