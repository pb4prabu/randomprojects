package com.oauth.server.oauthserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OauthServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(OauthServerApplication.class, args);
		
	}
	
//	@Autowired 
//	UserDetailsService service;
//	@Autowired
//	public void authenticationManager(AuthenticationManagerBuilder builder) throws Exception {
//	    builder.userDetailsService(service);
//	}
	
}
