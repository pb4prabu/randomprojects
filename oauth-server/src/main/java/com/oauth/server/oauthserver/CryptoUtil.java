//
//package com.oauth.server.oauthserver;
//
//import org.jasypt.util.text.StrongTextEncryptor;
//import org.springframework.security.crypto.bcrypt.BCrypt;
//
//public class CryptoUtil
//{
//
//    public static String bCrypt(String password)
//    {
//	String randomSalt = BCrypt.gensalt();
//
//	String encryptedPassword = BCrypt.hashpw(password , randomSalt);
//	return encryptedPassword;
//    }
//
//    public static boolean verify(String passwordParam , String originalPassword)
//    {
//	String haString = BCrypt.hashpw(passwordParam , originalPassword);
//	return haString.equals(originalPassword);
//    }
//
//    public static String strongTxtEncryption(String password , String salt)
//    {
//	StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
//	textEncryptor.setPassword(salt);
//	String myEncryptedText = textEncryptor.encrypt(password);
//	return myEncryptedText;
//    }
//
//    public static String strongTxtDecryption(String password , String salt)
//    {
//	StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
//	textEncryptor.setPassword(salt);
//	return textEncryptor.decrypt(password);
//    }
//
//    public static void main(String[] args)
//    {
//
//	String username = "e2hHNdFX8PyS6Y06eniDJQ==";
//	String password = "OxNnAwKLuPnBltOcBk1x8TWmb+aHlGwx";
//	boolean encryption = false; // true for encryption false for decryption
//
//	if (encryption)
//	{
//	    StrongTextEncryptor unameEncryptor = new StrongTextEncryptor();
//	    unameEncryptor.setPassword(password);
//	    String myEncryptedusername = unameEncryptor.encrypt(username);
//
//	    System.out.println("userName : " + myEncryptedusername);
//
//	    StrongTextEncryptor passEncryptor = new StrongTextEncryptor();
//	    passEncryptor.setPassword(myEncryptedusername);
//	    String myEncryptedPass = passEncryptor.encrypt(password);
//
//	    System.out.println("password : " + myEncryptedPass);
//	}
//	else
//	{
//	    StrongTextEncryptor passwordDecryptor = new StrongTextEncryptor();
//	    passwordDecryptor.setPassword(username);
//	    password = passwordDecryptor.decrypt(password);
//
//	    StrongTextEncryptor unameDecryptor = new StrongTextEncryptor();
//	    unameDecryptor.setPassword(password);
//	    username = unameDecryptor.decrypt(username);
//
//	    System.out.println("userName : " + username);
//	    System.out.println("password : " + password);
//
//	}
//
//    }
//
//}
