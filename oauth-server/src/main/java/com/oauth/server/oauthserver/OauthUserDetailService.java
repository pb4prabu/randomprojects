package com.oauth.server.oauthserver;

import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
@Service
public class OauthUserDetailService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		String userName = null;
		String password = null;
		Set<OauthRole> oauthRole = new HashSet<>();

		switch (username) {
		case "user1": {
			userName = "user1";
			password = "user1pass";
			oauthRole.add(OauthRole.ROLE_USER);
			break;
		}
		case "user2": {
			userName = "user2";
			password = "user2pass";
			oauthRole.add(OauthRole.ROLE_USER);
			break;
		}
		case "partner1": {
			userName = "partner1";
			password = "partner1pass";
			oauthRole.add(OauthRole.ROLE_PARTNER);
			break;
		}
		case "partner2": {
			userName = "partner2";
			password = "partner2pass";
			oauthRole.add(OauthRole.ROLE_PARTNER);
			break;
		}
		case "admin1": {
			userName = "admin1";
			password = "admin1pass";
			oauthRole.add(OauthRole.ROLE_PARTNER);
			oauthRole.add(OauthRole.ROLE_ADMIN);
			oauthRole.add(OauthRole.ROLE_USER);
			break;
		}
		case "admin2": {
			userName = "admin2";
			password = "admin2pass";
			oauthRole.add(OauthRole.ROLE_PARTNER);
			oauthRole.add(OauthRole.ROLE_ADMIN);
			oauthRole.add(OauthRole.ROLE_USER);
			oauthRole.add(OauthRole.ROLE_SUPER_ADMIN);
			break;
		}
		default: {
			throw new UsernameNotFoundException(String.format("User %s does not exist!", username));
		}
		}
		return new OauthUserDetails(userName, password, oauthRole);
	}

}
