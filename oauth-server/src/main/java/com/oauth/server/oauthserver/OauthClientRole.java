package com.oauth.server.oauthserver;

import org.springframework.security.core.GrantedAuthority;

public enum OauthClientRole implements GrantedAuthority {

	ROLE_TRUSTED_CLIENT,ROLE_TRUSTED_CLIENT_TEMP;

	@Override
	public String getAuthority() {
		return this.toString();
	}

	
}
