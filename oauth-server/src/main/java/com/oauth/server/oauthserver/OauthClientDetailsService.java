package com.oauth.server.oauthserver;

import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableSet;

@Component
@Primary
public class OauthClientDetailsService implements ClientDetailsService {

	@Override
	public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {

		// clients.inMemory().withClient("trusted-app")
		// .authorizedGrantTypes("client_credentials", "password",
		// "refresh_token")
		// .authorities("ROLE_TRUSTED_CLIENT").scopes("read",
		// "write").resourceIds(resourceId)
		// .accessTokenValiditySeconds(accessTokenValiditySeconds)
		// .refreshTokenValiditySeconds(refreshTokenValiditySeconds).secret("secret");

		System.out.println(
				"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ enter into client");

		return new OauthClientDetails("trusted-app", "secret", ImmutableSet.of("read", "write"),
				ImmutableSet.of("client_credentials", "password", "refresh_token"),
				ImmutableSet.of(OauthClientRole.ROLE_TRUSTED_CLIENT), 7200, 5184000);
	}

}
